FROM alpine

RUN apk add --no-cache openldap openldap-clients openldap-back-mdb ldapvi

EXPOSE 389

VOLUME ["/etc/openldap-dist", "/var/lib/openldap"]

COPY modules/ /etc/openldap/modules

COPY entrypoint.sh /entrypoint.sh

ENTRYPOINT ["/entrypoint.sh"]

CMD ["slapd", "-d", "32768", "-u", "ldap", "-g", "ldap"]
